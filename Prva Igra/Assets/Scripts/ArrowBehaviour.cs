﻿using System.Collections;
using UnityEngine;

public class ArrowBehaviour : MonoBehaviour {

    public GameObject mouse;

    private float screenWidth, screenHeight, arrowWidth, arrowHeight, mouseWidth, mouseHeight, transparencyNumber = 0.75f;

    /*Funckije*/
    void SetWidthsAndHeights(){
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        arrowWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        arrowHeight = GetComponent<SpriteRenderer>().bounds.size.y;

        mouseWidth = mouse.GetComponent<SpriteRenderer>().bounds.size.x;
        mouseHeight = mouse.GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void SetPositionOutsideTheScreen(){
        transform.position = 2.5f * new Vector3(screenWidth, screenHeight, 0f);
    }

    void ArrowBehaviourInScreen(){
        transform.position = new Vector3(-screenWidth / 2f + 1.5f * mouseWidth, -screenHeight /2f + 2f * mouseHeight + arrowHeight/2f, 0);

        if (GetComponent<SpriteRenderer>().color.a >= 1 || GetComponent<SpriteRenderer>().color.a <= 0.1f)
            transparencyNumber *= -1;
        GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, transparencyNumber * Time.deltaTime);
    }

    //////////


	void Awake () {
        SetWidthsAndHeights();
        //&&
        SetPositionOutsideTheScreen();
	}
	
	void FixedUpdate () {
		if(CheesePosition.points == 20){
            ArrowBehaviourInScreen();
        }
	}
}
