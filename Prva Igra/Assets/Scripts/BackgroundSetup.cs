﻿using UnityEngine;
using System.Collections;

public class BackgroundSetup : MonoBehaviour {

    private float screenWidth, screenHeight;
    private Vector3 screenValueInWorldPoints;


    /*Funkcije*/
    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;
    }

    void SetBackground()
    {
        transform.localScale = new Vector3(1f, 1f, 1f); ///ovo moram namjestiti zbog doljnjeg skaliranja
        transform.position = new Vector3(0, 0, 10); ///ovo moram namjestiti zbog doljnjeg odredivanja pozicije

        float widthBeforeScaling = GetComponent<SpriteRenderer>().bounds.size.x;
        float heightBeforeScaling = GetComponent<SpriteRenderer>().bounds.size.y;
        float spaceBar = 14f / 15f;

        transform.localScale = new Vector3(screenWidth / widthBeforeScaling, spaceBar * screenHeight / heightBeforeScaling, 1);
        transform.position -= new Vector3(0, (1f - spaceBar) / 2f * screenHeight, 0);

        SetPosition();
    }

    void SetPosition()
    {
        if(name == "Level 2 - Background"){
            transform.position -= new Vector3(GetComponent<SpriteRenderer>().bounds.size.x, 0, 0);
        }

        if (name == "Level 3 - Background"){
            transform.position -= 2 * new Vector3(GetComponent<SpriteRenderer>().bounds.size.x, 0, 0);
        }

        if (name == "Level 4 - Background"){
            transform.position -= 3 * new Vector3(GetComponent<SpriteRenderer>().bounds.size.x, 0, 0);
        }

        if (name == "Level 5 - Background"){
            transform.position -= 4 * new Vector3(GetComponent<SpriteRenderer>().bounds.size.x, 0, 0);
        }
    }


    void Awake()
    {
        SetWidthsAndHeights();
        //&&
        SetBackground();
    }
	
}
