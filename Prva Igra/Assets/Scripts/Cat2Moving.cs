﻿using UnityEngine;
using System.Collections;

public class Cat2Moving : MonoBehaviour
{

    public GameObject mouse, cheese, strawberry, mainCamera;
    public float speed = 4.5f;
    public static int pointsForCat2Time = 80;
    public float getBackSpeed; //ovo mi treba za ShoePosition

    private int cat2Level, cat2FirstLevel = 4;
    private Vector3 direction, vectorToTarget, randomDirection;
    private float angle, secondAngle, rotationSpeed = 4f, screenWidth, screenHeight, cat2Width, cat2Height, leftBorder, rightBorder;
    private bool goForCheese, goForStrawberry, setRandomDirection = true, cat2Time = false, passingThroughLevels = false,
        flagForCat2FirstMood = true, moveNow = false, cheeseOrStrawberryMoodFlag = true, otherMoodFlag = true,
         goRandomAfterMood = false;
    private Vector3 range;
    private Quaternion targetRotation;

    /*Timeri*/
    private float cheeseOrStrawberryMoodTimer = 2f, beforeRandomMovingTimer = 1.15f;
    ///

    /*Funkcije*/
    void SetGetBackSpeed()
    {
        getBackSpeed = speed;
    }

    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        cat2Width = GetComponent<SpriteRenderer>().bounds.size.x;
        cat2Height = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void SetRange()
    {
        range = new Vector3(screenWidth / 2f, screenHeight / 2f - cat2Width / 2f, 0);
    }

    void SetFirstPosition()
    {
        transform.position = new Vector3( (1 - 2 * cat2FirstLevel) * screenWidth / 2f + cat2Width, screenHeight / 2f - cat2Height - StatusBar.statusBarHeight, 0f);
    }


    IEnumerator Cat2MoodBeforeCheese(float timeCounter)
    {
        GetComponent<Animator>().Play("Cat2_Idle");
        yield return new WaitForSeconds(timeCounter);
        direction = cheese.transform.position;
        SettingVariablesToConnectBehaviours("cheese");
    }

    IEnumerator Cat2MoodBeforeStrawberry(float timeCounter)
    {
        GetComponent<Animator>().Play("Cat2_Idle");
        yield return new WaitForSeconds(timeCounter);
        direction = strawberry.transform.position;
        SettingVariablesToConnectBehaviours("strawberry");
    }

    IEnumerator Cat2MoodBeforeRandomMoving(float timeCounter)
    {
        GetComponent<Animator>().Play("Cat2_Gets_Angry");
        yield return new WaitForSeconds(timeCounter);
        goRandomAfterMood = true;
    }

    bool isCheeseInTheScreen()
    {
        if (cheese.transform.position.x >= (1 - 2 * cat2Level) * screenWidth / 2f && cheese.transform.position.x <= (3 - 2 * cat2Level) * screenWidth / 2f && cheese.transform.position.y >= -screenHeight / 2f && cheese.transform.position.y <= screenHeight / 2f - StatusBar.statusBarHeight)
            return true;
        return false;
    }

    bool isStrawberryInTheScreen()
    {
        if (strawberry.transform.position.x >= (1 - 2 * cat2Level) * screenWidth / 2f && strawberry.transform.position.x <= (3 - 2 * cat2Level) * screenWidth / 2f && strawberry.transform.position.y >= -screenHeight / 2f && strawberry.transform.position.y <= screenHeight / 2f - StatusBar.statusBarHeight)
            return true;
        return false;
    }

    Vector3 PositionForSwitchingLevels()
    {
        return new Vector3((1 - 2 * cat2Level) * screenWidth / 2f + 3f * cat2Width / 2f, -screenHeight / 2f + cat2Height / 2f, 0f);
    }

    void Cat2Level()
    {
        if (transform.position.x > -screenWidth / 2f && transform.position.x < screenWidth / 2f)
            cat2Level = 1;
        if (transform.position.x > -3f * screenWidth / 2f && transform.position.x < -screenWidth / 2f)
            cat2Level = 2;
        if (transform.position.x > -5f * screenWidth / 2f && transform.position.x < -3f * screenWidth / 2f)
            cat2Level = 3;
        if (transform.position.x > -7f * screenWidth / 2f && transform.position.x < -5f * screenWidth / 2f)
            cat2Level = 4;
        if (transform.position.x > -9f * screenWidth / 2f && transform.position.x < -7f * screenWidth / 2f)
            cat2Level = 5;
    }

    void BorderValues()
    {
        leftBorder = (1 - 2 * cat2Level) * screenWidth / 2f + cat2Width / 2f;
        rightBorder = (3 - 2 * cat2Level) * screenWidth / 2f - cat2Width / 2f;
    }

    void SettingVariablesToConnectBehaviours(string nameOfObject)
    {
        if(nameOfObject == "cheese" || nameOfObject == "strawberry"){
            otherMoodFlag = true;
            goRandomAfterMood = false;
            setRandomDirection = true;
            passingThroughLevels = true;
        }

        else{
            cheeseOrStrawberryMoodFlag = true;
            passingThroughLevels = true;
        }
    }

    void CatsDirection()
    {
        goForCheese = isCheeseInTheScreen();
        goForStrawberry = isStrawberryInTheScreen();

        if (goForCheese){ //ako je sir u ekranu kao i macka idi za sirom
            if (cheeseOrStrawberryMoodFlag){
                direction = transform.position;
                StartCoroutine(Cat2MoodBeforeCheese(cheeseOrStrawberryMoodTimer));
                cheeseOrStrawberryMoodFlag = false;
            }
        }

        else if (goForStrawberry){ //ako je jagoda u ekranu kao i macka idi za jagodom
            if(cheeseOrStrawberryMoodFlag){ 
                direction = transform.position;
                StartCoroutine(Cat2MoodBeforeStrawberry(cheeseOrStrawberryMoodTimer));
                cheeseOrStrawberryMoodFlag = false;
            }
        }
        
        else if(cat2Level == mainCamera.GetComponent<MainCameraSettings>().mouseLevel) { //inace, ako je mis u ekranu sa mackom
            //idi random
            SettingVariablesToConnectBehaviours("random");

            if(otherMoodFlag){
                direction = transform.position;
                StartCoroutine(Cat2MoodBeforeRandomMoving(beforeRandomMovingTimer));
                otherMoodFlag = false;
            }

            if (goRandomAfterMood){

                if (setRandomDirection)
                { //ovdje odredujem smjer kretanja od mogucih 4
                    randomDirection = new Vector3(mouse.transform.position.x - transform.position.x, mouse.transform.position.y - transform.position.y, 0);
                    randomDirection = new Vector3(Mathf.Sign(randomDirection.x) * 1f, Mathf.Sign(randomDirection.y) * 1f, 0);

                    direction = transform.position + randomDirection;

                    setRandomDirection = false;
                }

                else
                {
                    if (transform.position.x == leftBorder || transform.position.x == rightBorder)
                        randomDirection = new Vector3(-1f * randomDirection.x, randomDirection.y, 0);
                    if (transform.position.y == range.y - StatusBar.statusBarHeight || transform.position.y == -range.y)
                        randomDirection = new Vector3(randomDirection.x, -1f * randomDirection.y, 0);

                    direction = transform.position + randomDirection;
                }
            }

        }

        else{ //a ako nista od toga nije zadovoljeno, idi na drugi level
            if (transform.position != PositionForSwitchingLevels() && passingThroughLevels)
                direction = PositionForSwitchingLevels();

            else{
                passingThroughLevels = false;
                direction = new Vector3((1 - 2 * cat2Level) * screenWidth / 2f - cat2Width / 2f, -screenHeight / 2f + cat2Height / 2f, 0f);
            }
        }
    }

    void CatsMoving()
    {
        transform.position = Vector3.MoveTowards(transform.position, direction, speed * Time.deltaTime);

        if (cat2Level == mainCamera.GetComponent<MainCameraSettings>().mouseLevel)
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, leftBorder, rightBorder), Mathf.Clamp(transform.position.y, -range.y, range.y - StatusBar.statusBarHeight), 0);
    }

    void CatsLooking()
    {
        if (goForCheese) vectorToTarget = cheese.transform.position - transform.position;
        else vectorToTarget = Vector3.up;

        angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
        targetRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
    }
    ///


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {
        SetGetBackSpeed();
        //&&
        SetWidthsAndHeights();
        //&&
        SetRange();
        //&&
        SetFirstPosition();

    }

    public void FixedUpdate()
    {
        if (mouse.GetComponent<MouseMoving>().gameIsOn)
        {
            Cat2Level(); //prebaci mozda u if(cat2Time), nesto sam skeptican u vezi toga. Isto to je i u CatMoving-u

            if (CheesePosition.points >= pointsForCat2Time && mainCamera.GetComponent<MainCameraSettings>().mouseLevel >= cat2FirstLevel)
                cat2Time = true;

            if (cat2Time){
                BorderValues();
                //&&
                CatsDirection();
                //&&
                CatsMoving();
                //&&
                CatsLooking();
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mouse"){
            StartCoroutine(mouse.GetComponent<MouseMoving>().GameOver());
        }
    }
}
