﻿using UnityEngine;
using System.Collections;

public class CatMoving : MonoBehaviour
{

    public GameObject mouse, mainCamera;
    public float speed = 3.5f, getBackSpeed; //ovo mi treba za ShoePosition
    public static int pointsForCatTime = 20;

    private int catLevel, catFirstLevel = 2;
    private float angle, secondAngle, rotationSpeed = 4f, screenWidth, screenHeight, catsWidth, catsHeight, catFirstMoodTime = 1f;
    private bool catTime = false, flagForCatFirstMood = true, moveNow = false, passingThroughLevels = false;
    private Vector3 direction, vectorToTarget;
    private Quaternion targetRotation;

    /*Funkcije*/
    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        catsWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        catsHeight = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void SetFirstPosition()
    {
        transform.position = new Vector3( (1 - 2 * catFirstLevel) * screenWidth / 2f + catsWidth, screenHeight / 2f - catsHeight/2f - StatusBar.statusBarHeight, 0f);
    }

    void SetSpeed()
    {
        getBackSpeed = speed;
    }


    Vector3 PositionForSwitchingLevels()
    {
        return new Vector3( (1 - 2 * catLevel) * screenWidth/2f + catsWidth / 2f, -screenHeight/2f + catsHeight / 2f, 0f);
    }

    void SetDirection()
    {
        if (catLevel == mainCamera.GetComponent<MainCameraSettings>().mouseLevel){
            passingThroughLevels = true;
            direction = mouse.transform.position;
        }

        else if (transform.position != PositionForSwitchingLevels() && passingThroughLevels)
            direction = PositionForSwitchingLevels();

        else {
            passingThroughLevels = false;
            direction = new Vector3((1 - 2 * catLevel) * screenWidth / 2f - catsWidth / 2f, -screenHeight / 2f + catsHeight / 2f, 0f);
        }
    }

    void MoveTowardsTheTarget()
    {
        transform.position = Vector3.MoveTowards(transform.position, direction, speed * Time.deltaTime);
    }

    void LookAtTarget()
    {
        /////nasao na internetu
        vectorToTarget = direction - transform.position;
        angle = Mathf.Atan2(vectorToTarget.y, vectorToTarget.x) * Mathf.Rad2Deg - 90;
        targetRotation = Quaternion.AngleAxis(angle, Vector3.forward);
        transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, Time.deltaTime * rotationSpeed);
    }

    void CatLevel()
    {
        if (transform.position.x > -screenWidth / 2f && transform.position.x < screenWidth / 2f)
            catLevel = 1;
        if (transform.position.x > -3f * screenWidth / 2f && transform.position.x < -screenWidth / 2f)
            catLevel = 2;
        if (transform.position.x > -5f * screenWidth / 2f && transform.position.x < -3f * screenWidth / 2f)
            catLevel = 3;
        if (transform.position.x > -7f * screenWidth / 2f && transform.position.x < -5f * screenWidth / 2f)
            catLevel = 4;
        if (transform.position.x > -9f * screenWidth / 2f && transform.position.x < -7f * screenWidth / 2f)
            catLevel = 5;
    }

    IEnumerator CatsFirstMood()
    {
        yield return new WaitForSeconds(0.5f);
        yield return new WaitForSeconds(catFirstMoodTime);
        transform.rotation = Quaternion.identity;
        moveNow = true;
    }
    /////


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {
        SetWidthsAndHeights();
        //&&
        SetSpeed();
    }

    void Start()
    {//stavljeno u Start a ne u Awake jer statusBarHeight ucitava kao 0 jer se valjda ova skripta izvrti prije skripte StatusBar
        SetFirstPosition(); //probaj naci mozda neko elegantnije rjesenje 
    }

 
    public void FixedUpdate()
    {
        if (mouse.GetComponent<MouseMoving>().gameIsOn)
        {
            CatLevel();

            if (CheesePosition.points >= pointsForCatTime && mainCamera.GetComponent<MainCameraSettings>().mouseLevel >= catFirstLevel)
                catTime = true;

            if (catTime){
                if (flagForCatFirstMood){
                    StartCoroutine(CatsFirstMood());
                    flagForCatFirstMood = false;
                }

                if (moveNow){
                    SetDirection();
                    //&&
                    MoveTowardsTheTarget();
                    //&&
                    LookAtTarget();
                }
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if(collision.gameObject.name == "Mouse")
        {
            StartCoroutine(mouse.GetComponent<MouseMoving>().GameOver());
        }
    }
}
