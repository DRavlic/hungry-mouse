﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class CheesePosition : MonoBehaviour
{
    public static int points;
    public GameObject mouse, cat, cat2, trap, shoe, mainCamera;
    public Text score;
    public bool firstCheese = true, cheeseTime = true, countTime = false;
    public int trueLevel;
    public Vector3 cheesePositionOutsideTheScreen;

    private float screenWidth, screenHeight, cheeseWidth, cheeseHeight, catWidth;
    private int hits = 0, numberOfHitsToChangeToStrawberry = 2, pointsToAddForCheese = 10;
    private Vector3 range;

    /*Timeri*/
    private float firstTimeCounter = 3.5f, timeCounter = 5f;
    //////

    /*Funkcije*/
    IEnumerator NewPosition(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        ///ovaj for sluzi da mi pronade poziciju koja nije blizu nijednom objektu
        while (true){
            Vector3 temporaryPosition = new Vector3(Random.Range( (1 - 2* trueLevel) * range.x + cheeseWidth / 2f, (3 - 2* trueLevel) * range.x - cheeseWidth / 2f), Random.Range(-range.y, range.y - StatusBar.statusBarHeight), 0);
            ///cijeli bool je ovdje
            bool positionIsGood = Vector3.Distance(temporaryPosition, mouse.transform.position) > 3 * catWidth
                && Vector3.Distance(temporaryPosition, cat.transform.position) > catWidth
                    && Vector3.Distance(temporaryPosition, trap.transform.position) > catWidth
                        && Vector3.Distance(temporaryPosition, shoe.transform.position) > catWidth
                            && Vector3.Distance(temporaryPosition, mouse.transform.position) < Vector3.Distance(temporaryPosition, cat2.transform.position);
            /// jos sam stavio uvjet da se sir uvijek pojavljuje blize misu nego 2. macki (razmisli to isto i za macku 1)
            if (positionIsGood){
                transform.position = temporaryPosition;
                break;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        cheeseWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        cheeseHeight = GetComponent<SpriteRenderer>().bounds.size.y;

        catWidth = cat.GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void SetRange()
    {
        range = new Vector3(screenWidth / 2f, screenHeight / 2f - cheeseHeight / 2f, 0);
    }

    void SetPoints()
    {
        points = 0;
    }

    void SetCheeseLevel()
    {
        if (points < CatMoving.pointsForCatTime) trueLevel = 1;
        else if (points < TrapPosition.pointsForTrapTime) trueLevel = 2;
        else if (points < Cat2Moving.pointsForCat2Time) trueLevel = 3;
        else if (points < ShoePosition.pointsForShoeTime) trueLevel = 4;
        else trueLevel = 5; 
                              //umetni i 6. level ako bude trebalo
    }


    void FirstCheesePosition()
    {
        if (firstCheese){
            StartCoroutine(NewPosition(firstTimeCounter));
            firstCheese = false;
        }
    }

    void OthersCheesesPosition()
    {
        if (countTime){
            StartCoroutine(NewPosition(timeCounter));
            countTime = false;
        }
    }

    void DoOnCollision()
    {
        SetPositionOutsideTheScreen();
        countTime = true;
    }

    void AddPoints()
    {
        points += pointsToAddForCheese;
        score.text = "POINTS : " + points.ToString();
    }

    void CheckNumberOfHits()
    {
        hits++;
        if (hits == numberOfHitsToChangeToStrawberry){
            cheeseTime = false;
            hits = 0;
        }
    }

    void SetPositionOutsideTheScreen()
    {
        transform.position = cheesePositionOutsideTheScreen;
    }
    ///////


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {

        SetWidthsAndHeights();
        //&&
        SetRange();
        //&&
        cheesePositionOutsideTheScreen = 2f * new Vector3(screenWidth, screenWidth, 0);
        SetPositionOutsideTheScreen();
    }

    void Start()
    {
        SetPoints();
    }

    void FixedUpdate()
    {
        SetCheeseLevel();

        if (cheeseTime && mouse.GetComponent<MouseMoving>().gameIsOn && trueLevel == mainCamera.GetComponent<MainCameraSettings>().mouseLevel)
        {
            FirstCheesePosition();
            //|                         
            //|                             
            //V                  //<-
            OthersCheesesPosition(); //|
                                  //|
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mouse"){
            DoOnCollision();
            //&&
            AddPoints();
            //&&
            CheckNumberOfHits();
        }

        if (collision.gameObject.name == "Cat"){
            DoOnCollision();
        }

        if (collision.gameObject.name == "Cat2") {
            DoOnCollision();
        }
    }
}
