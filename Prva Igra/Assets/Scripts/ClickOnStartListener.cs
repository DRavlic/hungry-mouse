﻿using System.Collections;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ClickOnStartListener : MonoBehaviour {

	void Start () {
		
	}
	
	void Update () {
               
        Vector2 mousePosition = Camera.main.ScreenToWorldPoint(Input.mousePosition); //tj pozicija prsta

        
        bool overSprite = GetComponent<SpriteRenderer>().bounds.Contains(mousePosition);

        
        if (overSprite){
            
            if (Input.GetButton("Fire1"))
            {
                SceneManager.LoadScene(1);
            }
        }
    }
}
