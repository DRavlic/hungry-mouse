﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class GoToMainMenu : MonoBehaviour
{

    public Text returnText;

    public void doOnClick()
    {
        SceneManager.LoadScene(0);
    }

    void Start()
    {
        gameObject.SetActive(false);
        returnText.gameObject.SetActive(false);
    }
}
