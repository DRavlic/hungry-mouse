﻿using System.Collections;
using UnityEngine;

public class HoleShadowBehaviour : MonoBehaviour {

    public GameObject mouse, cheese;

    private int level;
    private bool flagForAnimation = true;
    private float screenWidth, screenHeight, holeShadowWidth, holeShadowHeight, mouseWidth, mouseHeight, transparencyNumber = 0.35f;

    /*Funckije*/
    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        mouseWidth = mouse.GetComponent<SpriteRenderer>().bounds.size.x;
        mouseHeight = mouse.GetComponent<SpriteRenderer>().bounds.size.y;

        transform.localScale = new Vector3(1f, 1f, 1f);
        transform.localScale = new Vector3( 0.75f * mouseWidth / GetComponent<SpriteRenderer>().bounds.size.x, (mouseHeight * 2f) / GetComponent<SpriteRenderer>().bounds.size.y, 1f);

        holeShadowWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        holeShadowHeight = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void SetPositionOutsideTheScreen()
    {
        transform.position = 3.5f * new Vector3(screenWidth, screenHeight, 0f);
    }

    void SetTransparency()
    {
        GetComponent<SpriteRenderer>().color = new Color(0, 0, 0, transparencyNumber);
    }

    void HoleShadowBehaviourInScreen()
    {
        level = cheese.GetComponent<CheesePosition>().trueLevel - 1;
        if (flagForAnimation){
            GetComponent<Animator>().Play("HoleOpening");
            flagForAnimation = false;
        }
        transform.position = new Vector3( (1 - 2*level) * screenWidth / 2f + holeShadowWidth / 2f, -screenHeight / 2f + holeShadowHeight / 2f, 0);
    }
    ////////

    void Awake () {
        SetWidthsAndHeights();
        //&&
        SetPositionOutsideTheScreen();
        //&&
        SetTransparency();
	}
	
	void FixedUpdate () {
		if(CheesePosition.points == CatMoving.pointsForCatTime || CheesePosition.points == TrapPosition.pointsForTrapTime
            || CheesePosition.points == Cat2Moving.pointsForCat2Time || CheesePosition.points == ShoePosition.pointsForShoeTime)
        {
            HoleShadowBehaviourInScreen();
        }

        else
        {
            flagForAnimation = true;
        }
	}
}
