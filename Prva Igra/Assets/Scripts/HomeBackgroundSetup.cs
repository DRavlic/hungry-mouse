﻿using UnityEngine;
using System.Collections;

public class HomeBackgroundSetup : MonoBehaviour {

    private float screenWidth, screenHeight;

    //Funkcije
    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;
    }

    void SetHomeBackground()
    {
        transform.localScale = new Vector3(1, 1, 1);

        float widthBeforeScaling = GetComponent<SpriteRenderer>().bounds.size.x;
        float heightBeforeScaling = GetComponent<SpriteRenderer>().bounds.size.y;

        transform.localPosition = new Vector3(0, 0, 0);
        transform.localScale = new Vector3(screenWidth / widthBeforeScaling, screenHeight / heightBeforeScaling, 1);
    }

    void Awake ()
    {
        SetWidthsAndHeights();
        //&&
        SetHomeBackground();	
	}
	
}
