﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MainCameraSettings : MonoBehaviour {

    public static float screenWidth, screenHeight;
    public GameObject mouse, cat, cat2;
    public int mouseLevel;

    private float backgroundWidth;
    private bool startCatAnimation = true, startCat2Animation = true;

    //Funkcije
    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;
    }

    void GoBackToMainMenu()
    {
        if (Input.GetKey(KeyCode.Escape)){
            SceneManager.LoadScene(0);
        }
    }

    void SetCameraPosition()
    {
        //u ovisnosti o tome na kojoj pozadini je mis, mici kameru
        if(mouse.transform.position.x >= -screenWidth / 2f && mouse.transform.position.x <= screenWidth / 2f){
            transform.position = new Vector3(0f, 0f, -10f);
            mouseLevel = 1;
        }

        if (mouse.transform.position.x >= -3 * screenWidth / 2f && mouse.transform.position.x <= -screenWidth / 2f){
            transform.position = new Vector3(-screenWidth, 0f, -10f);
            mouseLevel = 2;
            if (startCatAnimation){
                cat.GetComponent<Animator>().Play("Cat_Gets_Angry");
                startCatAnimation = false;
            }
        }

        if (mouse.transform.position.x >= -5 * screenWidth / 2f && mouse.transform.position.x <= -3 * screenWidth / 2f){
            transform.position = new Vector3(-2 * screenWidth, 0f, -10f);
            mouseLevel = 3;
        }

        if (mouse.transform.position.x >= -7 * screenWidth / 2f && mouse.transform.position.x <= -5 * screenWidth / 2f){
            transform.position = new Vector3(-3 * screenWidth, 0f, -10f);
            mouseLevel = 4;
            if (startCat2Animation)
            {
                cat.GetComponent<Animator>().Play("Cat2_Gets_Angry");
                startCat2Animation = false;
            }
        }

        if (mouse.transform.position.x >= -9 * screenWidth / 2f && mouse.transform.position.x <= -7 * screenWidth / 2f){
            transform.position = new Vector3(-4 * screenWidth, 0f, -10f);
            mouseLevel = 5;
        }

    }

    void ScreenNeverSleep()
    {
        Screen.sleepTimeout = SleepTimeout.NeverSleep;
    }

    ///////////////////////////////
    void Awake()
    {
        mouseLevel = 1; //ne razumijem zasto samo gore kod inicijalizacije ne mogu staviti da je = 1 i to je to??
    }

    void Start ()
    {
        SetWidthsAndHeights();
        //&&
        ScreenNeverSleep();
    }
	
	void FixedUpdate ()
    {
        //vracanje natrag na Main menu
        GoBackToMainMenu();
        //&&
        SetCameraPosition();
    }
}
