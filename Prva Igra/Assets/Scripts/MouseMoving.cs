﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;


public class MouseMoving : MonoBehaviour{
    public float speed = 15f, maxAcceleration = 0.7f;
    public bool gameIsOn = true;
    public Button tryAgainButton, returnButton;
    public Text tryAgainText, returnText;
    public GameObject mainCamera, cheese;

    private float screenWidth, screenHeight, mouseWidth, mouseHeight;
    private bool inArea;
    private Vector3 range, direction, theAcceleration, fixedAcceleration;
    private Quaternion calibrationQuaternion;

    /*Funkcije*/
    void CalibrateAccelerometer()
    {
        Vector3 accelerationSnapshot = Input.acceleration;

        Quaternion rotateQuaternion = Quaternion.FromToRotation(new Vector3(0.0f, 0.0f, -1.0f), accelerationSnapshot);

        calibrationQuaternion = Quaternion.Inverse(rotateQuaternion);
    }

    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        mouseWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        mouseHeight = GetComponent<SpriteRenderer>().bounds.size.y;

    }

    void SetRange()
    {
        range = new Vector3(screenWidth / 2f, screenHeight / 2f - mouseHeight / 2f, 0); //nisi znao hoces li ovom funkcijom odredivat
    } //range misa ili ces to sve regulirati u SetMouseMoving(pa si ga tamo stavio), odluci kasnije sto je efikasnije
    //mozda kasnije sredis i vezu izedu mainCamere

    void SetFixedAcceleration()
    {
        theAcceleration = Input.acceleration;
        theAcceleration.x = Mathf.Clamp(theAcceleration.x, -2f * maxAcceleration, 2f * maxAcceleration); /// ne kuzim zasto sam morao
        theAcceleration.y = Mathf.Clamp(theAcceleration.y, -2f * maxAcceleration, 2f * maxAcceleration); /// staviti *2f ovdje
      
        fixedAcceleration = calibrationQuaternion * theAcceleration;
    }

    void SetMouseMoving(int mouseLevel, int trueLevel)
    {
        direction = fixedAcceleration;

        transform.position += (direction * Time.deltaTime * speed);
        if (!inArea)
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, (1 - 2 * mouseLevel) * range.x + mouseWidth / 2f, (3 - 2 * mouseLevel) * range.x - mouseWidth / 2f), Mathf.Clamp(transform.position.y, -range.y, range.y - StatusBar.statusBarHeight), 0);
        else
            transform.position = new Vector3(Mathf.Clamp(transform.position.x, (1 - 2 * trueLevel) * range.x + mouseWidth / 2f, (3 - 2 * (trueLevel - 1)) * range.x - mouseWidth / 2f), Mathf.Clamp(transform.position.y, -range.y, range.y - StatusBar.statusBarHeight), 0);
    }

    void CheckPositionBetweenLevels(int level)
    {
        if (PointsBetweenLevels(CheesePosition.points) && transform.position.x <= (1 - 2 * level) * screenWidth / 2f + 2f * mouseWidth && transform.position.y <= -screenHeight / 2f + 2f * mouseHeight)
            inArea = true;
        else
            inArea = false;
    }

    public bool PointsBetweenLevels(int points) //public je jer ovu fju koristim u Cat2Moving-u
    {
        if (points == CatMoving.pointsForCatTime) return true; //za prvu macku
        if (points == TrapPosition.pointsForTrapTime) return true; //za klopke
        if (points == Cat2Moving.pointsForCat2Time) return true; //za drugu macku
        if (points == ShoePosition.pointsForShoeTime) return true; //za cipele
                                                                //za nesto drugo mozda
        return false;
    }

    public IEnumerator GameOver()
    {
        speed = 0;
        gameIsOn = false;
        yield return new WaitForSeconds(2f);
        GameOverMenu();
    }

    void GameOverMenu()
    {
        tryAgainButton.gameObject.SetActive(true);
        tryAgainText.gameObject.SetActive(true);

        returnButton.gameObject.SetActive(true);
        returnText.gameObject.SetActive(true);
    }
    //////


    void Awake()
    {
        inArea = false;
        CalibrateAccelerometer();
        //&&
        SetWidthsAndHeights();
        //&&
        SetRange();
    }

    public void FixedUpdate()
    {
        SetFixedAcceleration();
        //&&
        SetMouseMoving(mainCamera.GetComponent<MainCameraSettings>().mouseLevel, cheese.GetComponent<CheesePosition>().trueLevel);
        //&&
        CheckPositionBetweenLevels(mainCamera.GetComponent<MainCameraSettings>().mouseLevel);
    }
}
