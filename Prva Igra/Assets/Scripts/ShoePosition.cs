﻿using UnityEngine;
using System.Collections;

public class ShoePosition : MonoBehaviour
{

    public GameObject shoePrint, mouse, cat, cat2, cheese, trap;
    public static int pointsForShoeTime = 100;

    private float shoeHeight, speed = 0.4f, printToShoeScale = 0.3f, catsMoodTime = 2f, frameTime, screenWidth, screenHeight,
        leftBorder, rightBorder;
    private bool shoeTime = false, firstShoe = true; 
    private Vector3 range, shoeFirstPosition, shoeShadowFirstPosition;
    private int trueLevel;

    /*Timeri*/
    private float firstTimeCounter = 2f, timeCounter = 5f, timeOnGroundCounter = 2f;
    ///

    /*Funkcije*/
    IEnumerator NewPosition(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        ///ovaj for sluzi da mi pronade poziciju koja nije blizu nijednom objektu
        while(true){
            trueLevel = cheese.GetComponent<CheesePosition>().trueLevel;

            leftBorder = (1 - 2 * trueLevel) * range.x + 3f * shoeHeight / 4f;
            rightBorder = (3 - 2 * trueLevel) * range.x - 3f * shoeHeight / 4f;

            Vector3 temporaryPosition = new Vector3(Random.Range( leftBorder, rightBorder), Random.Range(-range.y, range.y - StatusBar.statusBarHeight), 0);
            ///cijeli bool je ovdje
            bool positionIsGood = Vector3.Distance(temporaryPosition, mouse.transform.position) > 1.5f * shoeHeight
                && Vector3.Distance(temporaryPosition, trap.transform.position) > 2.5f * shoeHeight; //gleda se sredina objekta pa mozda ne
            ///                                                                                  //bude dobro, provjeri to

            if (positionIsGood){
                shoePrint.transform.localScale = new Vector3(0.7f, 0.7f);
                shoePrint.transform.rotation = Quaternion.Euler(0, 0, Random.Range(-80f, 80f));
                shoePrint.transform.position = temporaryPosition;
                break;
            }
            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator MakeCatMoveAgain(int catNumber)
    {
        transform.position = shoeFirstPosition;

        if (catNumber == 1){
            cat.GetComponent<CatMoving>().speed = 0;
            yield return new WaitForSeconds(catsMoodTime);
            cat.GetComponent<CatMoving>().speed = cat.GetComponent<CatMoving>().getBackSpeed;
        }

        if (catNumber == 2){
            cat2.GetComponent<Cat2Moving>().speed = 0;
            yield return new WaitForSeconds(catsMoodTime);
            cat2.GetComponent<Cat2Moving>().speed = cat2.GetComponent<Cat2Moving>().getBackSpeed;
        }
    }

    IEnumerator ColliderOnTriggererOff(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);
        GetComponent<PolygonCollider2D>().isTrigger = false;
    }

    IEnumerator BehaveiorAfterTimeOnGround(float timeCounter1) //stavio jedinicu kraj njega jer se tak sto zove globalna var
    {
        yield return new WaitForSeconds(timeCounter1);
        if (mouse.GetComponent<MouseMoving>().gameIsOn) //jer bilo bi glupo da se mice cipela dok igrac bira hoce li izaci ili
        {                                               //nastaviti igru
            transform.position = shoeFirstPosition;

            ///zapocnimo sa odbrojavanjem do sljedeceg pada cipele
            StartCoroutine(NewPosition(timeCounter));
        }
    }

    void CaptureTheFrame()
    {
        frameTime = Time.deltaTime;
    }

    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        shoeHeight = GetComponent<SpriteRenderer>().bounds.size.y;
    }

    void SetRange()
    {
        range = new Vector3(screenWidth / 2f, screenHeight / 2f - shoeHeight / 2f, 0);
    } 

    void ShoeShadowFallingDown() ////aktivira se odmah nakon poziva newPosition jer on stavlja scalu na 0.7,0.7,0
    {
        if (shoePrint.transform.localScale.x > printToShoeScale && shoePrint.transform.localScale.y > printToShoeScale)
            shoePrint.transform.localScale -= new Vector3(Time.deltaTime * speed, Time.deltaTime * speed, 0);

        else if (shoePrint.transform.position != shoeShadowFirstPosition)
            SetShoeOnGround();
    } 

    void SetShoeOnGround()
    {
        ///potrebno zbog cipele i razlike izmedu njenog udarca u pod i stajanja na podu
        GetComponent<PolygonCollider2D>().isTrigger = true;

        ///zelimo rotaciju i poziciju cipele uskladiti sa sjenom
        transform.rotation = shoePrint.transform.rotation;
        transform.position = shoePrint.transform.position;

        ///mici sjenu
        shoePrint.transform.position = shoeShadowFirstPosition;

        ///cipela vise ne pada nego stoji na podu
        ShoeOnGroundBehave();
    }

    void ShoeOnGroundBehave()
    {
        StartCoroutine(ColliderOnTriggererOff(frameTime));

        ///pokreni brojac za vrijeme provedeno u ekranu i napravi nesto ako to istekne
        StartCoroutine(BehaveiorAfterTimeOnGround(timeOnGroundCounter));
    }

    void SetFirstPositions()
    {
        shoeShadowFirstPosition = 4f * new Vector3(screenWidth, screenHeight, 0);
        shoeFirstPosition = 4.5f * new Vector3(screenWidth, screenHeight, 0);

        shoePrint.transform.position = shoeShadowFirstPosition;
        transform.position = shoeFirstPosition;
    }
    ///


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {
        CaptureTheFrame();
        //&&
        SetWidthsAndHeights();
        //&&
        SetRange();
        //&&
        SetFirstPositions();
    }

    void FixedUpdate()
    {
        if (mouse.GetComponent<MouseMoving>().gameIsOn)
        {
            if (CheesePosition.points >= pointsForShoeTime)
                shoeTime = true;

            if (shoeTime){
                if (firstShoe){
                    StartCoroutine(NewPosition(firstTimeCounter));
                    firstShoe = false;
                }
                
                ShoeShadowFallingDown();   ///ova fja se aktivira nakon fje NewPosition i nakon sebe aktivira sve druge u nizu gdje          
            }                           ///zadnja fja aktivira NewPosition i opet je sve isto
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.name == "Mouse"){
            StartCoroutine(mouse.GetComponent<MouseMoving>().GameOver());
        }

        else if (other.gameObject.name == "Cat"){
            StartCoroutine(MakeCatMoveAgain(1));
        }

        else if (other.gameObject.name == "Cat2"){
            StartCoroutine(MakeCatMoveAgain(1));
        }

        else if (other.gameObject.name == "Cheese"){
            cheese.transform.position = cheese.GetComponent<CheesePosition>().cheesePositionOutsideTheScreen;
            cheese.GetComponent<CheesePosition>().countTime = true;
        }
    }

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mouse"){
            StartCoroutine(mouse.GetComponent<MouseMoving>().GameOver());
        }
    }
}
