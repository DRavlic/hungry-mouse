﻿using UnityEngine;
using System.Collections;

public class StatusBar : MonoBehaviour {

    public GameObject background;
    public static float statusBarHeight;

    void Awake()
    {
        float screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;
        statusBarHeight = screenHeight - background.GetComponent<SpriteRenderer>().bounds.size.y;

        float backgroundWorldSizeHeight = background.GetComponent<SpriteRenderer>().bounds.size.y;
        float backgroundPixelSizeHeight = Camera.main.WorldToScreenPoint(new Vector3(0, backgroundWorldSizeHeight / 2f, 0)).y; //ne znam zasto sve funkcionira da usporedujem sa pola sirine i visine ekrana

        backgroundPixelSizeHeight -= (Screen.height - backgroundPixelSizeHeight); //zbog toga sam isao ovom logikom
        GetComponent<RectTransform>().offsetMin = new Vector2(GetComponent<RectTransform>().offsetMin.x, backgroundPixelSizeHeight);
    }

}
