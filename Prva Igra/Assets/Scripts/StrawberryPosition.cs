﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class StrawberryPosition : MonoBehaviour {

    public static bool hitMouse = false, hitCat = false, hitCat2 = false;
    public Text score;
    public GameObject mouse, cat, cat2, trap, shoe, cheese, mainCamera;

    private float screenWidth, screenHeight, mouseAcceleration = 4.5f, catAcceleration = 2f, maxAccelerationBooster = 0.1f, catWidth,
        strawberryWidth, strawberryHeight;
    private bool countTimeToSlowThem;
    private int trueLevel, pointsToAddForStrawberry = 20;
    private Vector3 range;

    /*Timeri*/
    private float timeCounter = 5f, timeWhenTheySpeedUp = 3f, leftBorder, rightBorder;
    ///
       
    /*Funkcije*/
    IEnumerator NewPosition(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        ///ovaj for sluzi da mi pronade poziciju koja nije blizu nijednom objektu
        while (true)
        {
            trueLevel = cheese.GetComponent<CheesePosition>().trueLevel;

            leftBorder = (1 - 2 * trueLevel) * range.x + strawberryWidth / 2f;
            rightBorder = (3 - 2 * trueLevel) * range.x - strawberryWidth / 2f;

            Vector3 temporaryPosition = new Vector3(Random.Range( leftBorder, rightBorder), Random.Range(-range.y, range.y - StatusBar.statusBarHeight), 0);
            //cijeli bool je ovdje
            bool positionIsGood = Vector3.Distance(temporaryPosition, mouse.transform.position) > 3 * catWidth
                && Vector3.Distance(temporaryPosition, cat.transform.position) > catWidth
                    && Vector3.Distance(temporaryPosition, trap.transform.position) > catWidth
                        && Vector3.Distance(temporaryPosition, shoe.transform.position) > catWidth
                            && Vector3.Distance(temporaryPosition, mouse.transform.position) < Vector3.Distance(temporaryPosition, cat2.transform.position);
            //jos sam stavio uvjet da se sir uvijek pojavljuje blize misu nego 2. macki (razmisli to isto i za macku 1)
            if (positionIsGood)
            {
                transform.position = temporaryPosition;
                break;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator SlowThem(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        if (hitMouse){
            mouse.GetComponent<MouseMoving>().speed -= mouseAcceleration;
            mouse.GetComponent<MouseMoving>().maxAcceleration -= maxAccelerationBooster;
            hitMouse = false;
        }

        if (hitCat){
            if (cat.GetComponent<CatMoving>().speed != 0) cat.GetComponent<CatMoving>().speed -= catAcceleration;
            hitCat = false;
        }

        if (hitCat2){
            if (cat2.GetComponent<Cat2Moving>().speed != 0) cat2.GetComponent<Cat2Moving>().speed -= catAcceleration;
            hitCat2 = false;
        }
    }

    void SetWidthsAndHeights()
    {
        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        strawberryWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        strawberryHeight = GetComponent<SpriteRenderer>().bounds.size.y;

        catWidth = cat.GetComponent<SpriteRenderer>().bounds.size.x;
    }

    void SetRange()
    {
        range = new Vector3( screenWidth / 2f, screenHeight / 2f - strawberryHeight / 2f, 0);
    }

    void SetPositionOutsideTheScreen()
    {
        transform.position = 3f * new Vector3(screenWidth, screenHeight, 0f);
    }

    void SetStrawberryPosition()
    {
        if (cheese.GetComponent<CheesePosition>().countTime){
            StartCoroutine(NewPosition(timeCounter));
            cheese.GetComponent<CheesePosition>().countTime = false;
        }
    }

    void DoOnCollision()
    {
        if (hitMouse){
            mouse.GetComponent<MouseMoving>().speed += mouseAcceleration;
            mouse.GetComponent<MouseMoving>().maxAcceleration += maxAccelerationBooster;
        }

        if (hitCat){
            cat.GetComponent<CatMoving>().speed += catAcceleration;
        }

        if (hitCat2){
            cat2.GetComponent<Cat2Moving>().speed += catAcceleration;
        }

        SetPositionOutsideTheScreen();
        countTimeToSlowThem = true;

        cheese.GetComponent<CheesePosition>().cheeseTime = true;
        cheese.GetComponent<CheesePosition>().countTime = true;
    }

    void AddPoints()
    {
        CheesePosition.points += pointsToAddForStrawberry;
        score.text = "POINTS : " + CheesePosition.points.ToString();
    }
    ///


    void Awake()
    {
        SetWidthsAndHeights();
        //&&
        SetPositionOutsideTheScreen();
        //&&
        SetRange();
    }
	
	void FixedUpdate ()
    {
        if (mouse.GetComponent<MouseMoving>().gameIsOn && cheese.GetComponent<CheesePosition>().trueLevel == mainCamera.GetComponent<MainCameraSettings>().mouseLevel){

            if (!cheese.GetComponent<CheesePosition>().cheeseTime){
                SetStrawberryPosition();
            }

            if (countTimeToSlowThem){
                StartCoroutine( SlowThem(timeWhenTheySpeedUp) );
                countTimeToSlowThem = false;
            }
        }
            
	}

    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mouse"){
            hitMouse = true;
            DoOnCollision();
            //&&
            AddPoints();
        }

        if (collision.gameObject.name == "Cat"){
            hitCat = true;
            DoOnCollision();
        }

        if (collision.gameObject.name == "Cat2"){
            hitCat2 = true;
            DoOnCollision();
        }
    }
}
