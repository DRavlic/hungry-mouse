﻿using UnityEngine;
using System.Collections;

public class TrapPosition : MonoBehaviour {

    public GameObject mouse, cat, cat2, cheese, shoe, mainCamera;
    public static int pointsForTrapTime = 50;

    private float  mouseDeceleration = 3f, catDeceleration = 3f, maxAccelerationDetractor = 0.06f, screenWidth, screenHeight, catWidth, trapWidth, trapHeight, leftBorder, rightBorder,
            mouseWidth, mouseHeight;
    private bool hitMouse = false, hitCat = false, hitCat2 = false, firstTrap = true, inScreen, trapTime = false;
    private Vector3 range;
    private int trueLevel;

    /*Timeri*/
    private float firstTimeCounter = 5f, timeCounter = 4f, 
        inScreenCounter = 6f, getBackInScreenCounter, delayCounter = 3f, slowDownTime = 3f;
    /////

    /*Funkcije*/
    IEnumerator NewPosition(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        ///ovaj for sluzi da mi pronade poziciju koja nije blizu nijednom objektu
        while (true)
        {

            trueLevel = cheese.GetComponent<CheesePosition>().trueLevel;

            leftBorder = (1 - 2 * trueLevel) * range.x + trapWidth/2f + 3f/4f *mouseWidth;
            rightBorder = (3 - 2 * trueLevel) * range.x - trapWidth / 2f - 3f / 4f * mouseWidth;

            Vector3 temporaryPosition = new Vector3(Random.Range( leftBorder, rightBorder), Random.Range(-range.y, range.y - StatusBar.statusBarHeight), 0);
            ///cijeli bool je ovdje
            bool positionIsGood = Vector3.Distance(temporaryPosition, mouse.transform.position) > 2 * catWidth
                && Vector3.Distance(temporaryPosition, cat.transform.position) > 2 * catWidth
                    && Vector3.Distance(temporaryPosition, cat2.transform.position) > 2 * catWidth
                        && Vector3.Distance(temporaryPosition, cheese.transform.position) > catWidth
                            && Vector3.Distance(temporaryPosition, shoe.transform.position) > 3.5f * catWidth;
            ///
            if (positionIsGood)
            {
                transform.position = temporaryPosition;
                inScreen = true;
                break;
            }

            yield return new WaitForFixedUpdate();
        }
    }

    IEnumerator FastThem(float timeCounter)
    {
        yield return new WaitForSeconds(timeCounter);

        if (hitMouse){
            mouse.GetComponent<MouseMoving>().speed += mouseDeceleration;
            mouse.GetComponent<MouseMoving>().maxAcceleration += maxAccelerationDetractor;
            hitMouse = false;
        }

        if (hitCat){
            if (cat.GetComponent<CatMoving>().speed != 0) cat.GetComponent<CatMoving>().speed += catDeceleration;
            hitCat = false;
        }

        if (hitCat2){
            if (cat2.GetComponent<Cat2Moving>().speed != 0) cat2.GetComponent<Cat2Moving>().speed += catDeceleration;
            hitCat2 = false;
        }
    }

    void SetTimers()
    {
        getBackInScreenCounter = inScreenCounter;
    }

    void SetWidthsAndHeights()
    {
        trapWidth = GetComponent<SpriteRenderer>().bounds.size.x;
        trapHeight = GetComponent<SpriteRenderer>().bounds.size.y;

        screenWidth = Camera.main.ScreenToWorldPoint(new Vector3(Camera.main.pixelWidth, 0, 0)).x * 2f;
        screenHeight = Camera.main.ScreenToWorldPoint(new Vector3(0, Camera.main.pixelHeight, 0)).y * 2f;

        catWidth = cat.GetComponent<SpriteRenderer>().bounds.size.x;

        mouseWidth = mouse.GetComponent<SpriteRenderer>().bounds.size.x;
        mouseHeight = mouse.GetComponent<SpriteRenderer>().bounds.size.y; 
    }

    void SetRange()
    {
        ///malo je uzi izbor pozicije jer je glupo da klopka zavrsi na rubu ekrana
        range = new Vector3(screenWidth / 2f, screenHeight / 2f - trapHeight/2f - 3f/4f * mouseHeight, 0); 
    }

    void FirstTrapPosition()
    {
        if (firstTrap){
            StartCoroutine(NewPosition(firstTimeCounter));
            firstTrap = false;
        }
    }

    void IfNobodyGetsCaught()
    {
        if (inScreen){
            inScreenCounter -= Time.deltaTime;

            if (inScreenCounter < 0)
            {
                SetPositionOutsideTheScreen();

                StartCoroutine(NewPosition(delayCounter));
                inScreenCounter = getBackInScreenCounter;
            }
        }
    }

    void DoOnCollision()
    {
        if (hitMouse) {
            mouse.GetComponent<MouseMoving>().speed -= mouseDeceleration;
            mouse.GetComponent<MouseMoving>().maxAcceleration -= maxAccelerationDetractor;
        }

        if (hitCat){
            cat.GetComponent<CatMoving>().speed -= catDeceleration;
        }

        if (hitCat2){
            cat2.GetComponent<Cat2Moving>().speed -= catDeceleration;
        }

        SetPositionOutsideTheScreen();
        inScreenCounter = getBackInScreenCounter;

        ///ovdje vracamo brzine objektima dok klopka nije u ekranu
        StartCoroutine(FastThem(slowDownTime));

        ///ovdje stavljamo novu poziciju kako bi se klopka pojavila u ekranu
        StartCoroutine(NewPosition(timeCounter));
    }

    void SetPositionOutsideTheScreen()
    {
        transform.position = -2f * new Vector3(screenWidth, screenHeight, 0);
        inScreen = false;
    }
    //////


    //////////////////////////////////////////////////////////////////////////////////////////////////////
    void Awake()
    {
        SetTimers();
        //&&
        SetWidthsAndHeights();
        //&&
        SetRange();
        //&&
        SetPositionOutsideTheScreen();
    }

    void FixedUpdate()
    {
        if (mouse.GetComponent<MouseMoving>().gameIsOn)
        {
            if (CheesePosition.points >= pointsForTrapTime)
                trapTime = true;

            if (trapTime && cheese.GetComponent<CheesePosition>().trueLevel == mainCamera.GetComponent<MainCameraSettings>().mouseLevel)
            {
                FirstTrapPosition();
                //&&
                IfNobodyGetsCaught(); //mislim da ovo nece raditi kako bi ti zelio zbog 2. uvjeta u if-u gore jer cijelo vrijeme
                //ce biti na jednom mjestu kada mis treba preci na slijedeci level, ali kad gledas nije lose ovako jer sve sto sam
                //htio postici ovim pojavljivanjem na ekranu kada je i mis na istome ekranu je da igrac ne moze manipulirati igrom
                //tako da ceka na levelu iako moze prijeci na veci pa odmah pokupi sir ili jagodu koja se vec pojavila na levelu vise
            }
        }
    }
    //////////////////////////////////////////////////////////////////////////////////////////////////////


    public void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.name == "Mouse"){
            hitMouse = true;
            DoOnCollision();
        }

        if (collision.gameObject.name == "Cat"){
            hitCat = true;
            DoOnCollision();
        }

        if (collision.gameObject.name == "Cat2"){
            hitCat2 = true;
            DoOnCollision();
        }
    }

}
