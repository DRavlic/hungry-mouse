﻿using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using System.Collections;

public class TryAgain : MonoBehaviour
{ 
    public Text tryAgainText;

    public void doOnClick()
    {
        SceneManager.LoadScene(1);
    }

    void Start()
    {
        gameObject.SetActive(false);
        tryAgainText.gameObject.SetActive(false);
    }
}
